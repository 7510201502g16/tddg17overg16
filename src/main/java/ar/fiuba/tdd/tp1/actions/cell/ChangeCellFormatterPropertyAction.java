package ar.fiuba.tdd.tp1.actions.cell;

import ar.fiuba.tdd.tp1.actions.CellAction;
import ar.fiuba.tdd.tp1.elements.cell.Cell;

/**
 * Created by jonathan on 28/10/15.
 */
public class ChangeCellFormatterPropertyAction  extends CellAction{

    private String property;

    private String value;

    public ChangeCellFormatterPropertyAction(String sheetId, String row, Integer column, String property, String value) {
        super(sheetId, row, column);
        this.property = property;
        this.value = value;
    }

    @Override
    public void execute() {
        super.execute();
        final Cell cell = getCell();
        cell.getFormatter().changeProperty(property, value);
    }
}
