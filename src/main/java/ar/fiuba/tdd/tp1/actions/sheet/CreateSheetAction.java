package ar.fiuba.tdd.tp1.actions.sheet;

import ar.fiuba.tdd.tp1.actions.Action;

/**
 * Action executed when creating a sheet
 * Created by maxi on 30/09/15.
 */
public class CreateSheetAction extends Action {

    private String newSheetId;

    public CreateSheetAction(String newSheetId) {
        this.newSheetId = newSheetId;
    }

    @Override
    public void execute() {
        this.getContext().createSheet(this.newSheetId);
    }

    @Override
    public void undo() {
        this.getContext().deleteSheet(this.newSheetId);
    }
}
