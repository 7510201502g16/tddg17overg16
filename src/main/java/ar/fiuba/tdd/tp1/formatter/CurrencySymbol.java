package ar.fiuba.tdd.tp1.formatter;

/**
 * Created by jonathan on 14/10/15.
 */
public enum CurrencySymbol {

    AR("$"),
    EURO("€"),
    US("$"),
    YEN("¥"),
    REAL("R$"),
    USD("U$S");
    private String symbol;


    public static CurrencySymbol getBySymbol(String symbol) {
        CurrencySymbol currentSymbol = getCurrencySymbol(symbol);
        if (currentSymbol != null) {
            return currentSymbol;
        }
        throw new IllegalArgumentException(String.valueOf(symbol));
    }

    private static CurrencySymbol getCurrencySymbol(String symbol) {
        for (CurrencySymbol c : values()) {
            if (c.symbol.equals(symbol)) {
                return c;
            }
        }
        return null;
    }

    CurrencySymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
