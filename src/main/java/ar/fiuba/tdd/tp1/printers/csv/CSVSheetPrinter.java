package ar.fiuba.tdd.tp1.printers.csv;

import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.printer.PrintException;
import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.elements.printer.SheetPrinter;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;

/**
 * Created by maxi on 11/10/15.
 */
public class CSVSheetPrinter extends SheetPrinter {

    private Writer fileWriter;

    public CSVSheetPrinter(Sheet sheet, Writer fileWriter) {
        super(sheet);
        this.fileWriter = fileWriter;
    }

    @Override
    protected void printHeader(Sheet sheet) {
    }

    @Override
    protected void printRowEnd() {
        try {
            fileWriter.append("\n");
        } catch (IOException e) {
            throw new PrintException("Cannot print into file");
        }
    }

    @Override
    protected Printer getCellPrinter(Cell cell) {
        return new CSVCellPrinter(this.fileWriter, cell);
    }

    @Override
    protected Iterator<Iterator<Cell>> cellIterator(Sheet sheet) {
        return sheet.workingCellRangeIterator();
    }

    @Override
    protected void printCellSeparator() {
        try {
            fileWriter.append(",");
        } catch (IOException e) {
            throw new PrintException("Cannot print into file");
        }

    }

    @Override
    protected void printRowStart(Integer row) {

    }
}
