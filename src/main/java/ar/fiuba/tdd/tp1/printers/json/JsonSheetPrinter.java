package ar.fiuba.tdd.tp1.printers.json;

import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.elements.printer.SheetPrinter;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.printers.json.model.JsonCell;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by maxi on 12/10/15.
 */
public class JsonSheetPrinter extends SheetPrinter {

    private List<JsonCell> output;

    public JsonSheetPrinter(Sheet sheet, List<JsonCell> list) {
        super(sheet);
        this.output = list;
    }

    @Override
    protected void printHeader(Sheet sheet) {

    }

    @Override
    protected void printRowEnd() {

    }

    protected Printer getCellPrinter(Cell cell) {
        return new JsonCellPrinter(getSheet().getName(), cell, this.output);
    }

    @Override
    protected Iterator<Iterator<Cell>> cellIterator(Sheet sheet) {
        Iterator<Cell> cellIterator = getSheet().visitedCellsIterator();
        return Arrays.asList(cellIterator).iterator();
    }

    @Override
    protected void printCellSeparator() {

    }

    @Override
    protected void printRowStart(Integer row) {

    }

    /*    @Override
    public void print() {
        Iterator<Cell> iteratorIterator = sheet.visitedCellsIterator();
        while (iteratorIterator.hasNext()) {
            Cell next = iteratorIterator.next();
            Printer cellPrinter = getCellPrinter(next);
            cellPrinter.print();
        }
    }*/
}
