package ar.fiuba.tdd.tp1.queries.cell;

import ar.fiuba.tdd.tp1.queries.CellQuery;

/**
 * Query cell value
 * Created by jonathan on 3/10/15.
 */
public class CellValueQuery extends CellQuery<String> {

    public CellValueQuery(String sheetId, String col, Integer row) {
        super(sheetId, col, row);
    }

    @Override
    public String execute() {
        return getCell().getFormattedValue();
    }
}
