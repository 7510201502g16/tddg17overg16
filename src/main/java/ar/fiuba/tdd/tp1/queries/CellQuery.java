package ar.fiuba.tdd.tp1.queries;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.cell.ReadOnlyCellReference;
import ar.fiuba.tdd.tp1.elements.reference.ReferenceResolver;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.elements.sheet.SheetReference;

/**
 * Query cell attributes
 * Created by jonathan on 3/10/15.
 */
public abstract class CellQuery<T> extends Query<T> {

    private String sheetId;
    private Integer row;
    private String column;
    private ReferenceResolver<? extends Cell> reference;


    public CellQuery(String sheetId, String column, Integer row) {
        this.sheetId = sheetId;
        this.row = row;
        this.column = column;
    }

    @Override
    public void setContext(Book book) {
        super.setContext(book);
        completeReference();
    }

    private void completeReference() {
        ReferenceResolver<Sheet> sheetResolver = new SheetReference(this.sheetId, this.getContext());
        reference = new ReadOnlyCellReference(sheetResolver, this.column, this.row);
    }

    protected Cell getCell() {
        return reference.solve();
    }

}
