package ar.fiuba.tdd.tp1.queries;

import ar.fiuba.tdd.tp1.elements.printer.Printer;

/**
 * Created by jonathan on 15/10/15.
 */
public abstract class PrinterQuery extends Query<Printer> {


    protected abstract Printer createPrinter();

    @Override
    public Printer execute() {
        return createPrinter();
    }
}

