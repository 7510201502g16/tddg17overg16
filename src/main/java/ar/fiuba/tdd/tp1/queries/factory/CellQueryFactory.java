package ar.fiuba.tdd.tp1.queries.factory;

import ar.fiuba.tdd.tp1.queries.Query;

/**
 * Define the methods to be implemented to create book actions
 * Created by jonathan on 28/09/15.
 */
public interface CellQueryFactory {

    Query<String> getFormula();

    Query<String> getValue();
}
