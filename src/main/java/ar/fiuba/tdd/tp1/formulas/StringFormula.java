package ar.fiuba.tdd.tp1.formulas;

import ar.fiuba.tdd.tp1.elements.cell.Formula;

/**
 * Formula created when the string representing the formula is a string
 * Created by matias on 9/25/15.
 */
public class StringFormula implements Formula {
    //This could be merged with NumberFormula.
    private String stringValue;

    public StringFormula(String stringValue) {
        this.stringValue = stringValue;
    }

    @Override
    public String evaluate() {
        return this.stringValue;
    }

    @Override
    public String toString() {
        return this.stringValue;
    }

    @Override
    public String toStringAsRootFormula() {
        return this.toString();
    }
}
