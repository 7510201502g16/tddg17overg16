package ar.fiuba.tdd.tp1.formulas;

/**
 * Exception thrown when the formula is not well formed.
 * Created by matias on 9/25/15.
 */
public class InvalidFormulaException extends RuntimeException {

    public InvalidFormulaException(String message) {
        super(message);
    }
}
