package ar.fiuba.tdd.tp1.formulas.formulacreator;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.CellIdIterator;
import ar.fiuba.tdd.tp1.elements.cell.CellRange;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.RangeFunction;
import ar.fiuba.tdd.tp1.formulas.ReferenceFormula;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by matias on 10/17/15.
 */
public abstract class FunctionHandler extends CharHandler {

    protected static final Pattern rangePattern = Pattern.compile("^([a-zA-Z]+)([\\d]+):([a-zA-Z]+)([\\d]+)$");
    protected static final Pattern sheetRangePatter = Pattern.compile("^([^!]+)!([a-zA-Z]+[\\d]+:[a-zA-Z]+[\\d]+)$");
    protected Book book;
    protected Pattern parametersPattern;
    protected Map<String, RangeFunction> allowedFunctions;

    public FunctionHandler(Stack<Formula> formulaTree, Book activeBook) {
        super(formulaTree);
        this.book = activeBook;
    }

    protected CellRange getRange(String string) {
        Matcher matcher = rangePattern.matcher(string);
        if (matcher.matches()) {
            Integer startRow = Integer.parseInt(matcher.group(2));
            Integer endRow = Integer.parseInt(matcher.group(4));
            return new CellRange(matcher.group(1), startRow, matcher.group(3), endRow);
        } else {
            throw new InvalidRangeException();
        }
    }

    protected List<Formula> getReferenceList(String sheetId, CellRange range) {
        Iterator<String> cellIterator = new CellIdIterator(range);
        List<Formula> referenceList = new ArrayList<>();
        String currentCell;
        while (cellIterator.hasNext()) {
            currentCell = cellIterator.next();
            referenceList.add(new ReferenceFormula(sheetId.concat("!").concat(currentCell), this.book));
        }
        return referenceList;
    }
}
