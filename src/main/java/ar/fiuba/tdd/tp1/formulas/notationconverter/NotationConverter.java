package ar.fiuba.tdd.tp1.formulas.notationconverter;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Class that converts a string from infix notation to posfix.
 * Created by matias on 9/25/15.
 */
public final class NotationConverter {

    public static String convertToPosfix(String infix) {
        StringBuilder output = new StringBuilder();
        Deque<String> stack = new LinkedList<>();
        TokenHandler handler = new OperationHandler(output, stack);
        for (String token : infix.split("\\s")) {
            handler.handle(token);
        }
        while (!stack.isEmpty()) {
            output.append(stack.pop()).append(' ');
        }

        return output.toString();
    }

}
