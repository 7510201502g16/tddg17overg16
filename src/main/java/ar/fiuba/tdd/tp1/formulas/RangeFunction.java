package ar.fiuba.tdd.tp1.formulas;

import java.util.Iterator;
import java.util.List;

/**
 * Created by matias on 10/15/15.
 */
public enum RangeFunction {

    AVERAGE("AVERAGE") {
        @Override
        public String apply(List<String> rangeValues) {
            Float sum = 0.0f;
            for (String temp : rangeValues) {
                sum += Float.parseFloat(temp);
            }
            sum = sum / rangeValues.size();
            return sum.toString();
        }
    },
    MAX("MAX") {
        @Override
        public String apply(List<String> rangeValues) {
            Float max = Float.parseFloat(rangeValues.get(0));
            for (String temp : rangeValues) {
                Float current = Float.parseFloat(temp);
                if (current > max) {
                    max = current;
                }
            }
            return max.toString();
        }
    },
    MIN("MIN") {
        @Override
        public String apply(List<String> rangeValues) {
            Float min = Float.parseFloat(rangeValues.get(0));
            Iterator<String> iterator = rangeValues.iterator();
            while (iterator.hasNext()) {
                Float current = Float.parseFloat(iterator.next());
                if (current < min) {
                    min = current;
                }
            }
            return min.toString();
        }
    },
    CONCAT("CONCAT") {
        @Override
        public String apply(List<String> rangeValues) {
            String result = "";
            for (String current : rangeValues) {
                result = result.concat(current);
            }
            return result;
        }
    };

    private String functionName;

    RangeFunction(String name) {
        this.functionName = name;
    }

    public abstract String apply(List<String> rangeValues);

    @Override
    public String toString() {
        return this.functionName;
    }
}
