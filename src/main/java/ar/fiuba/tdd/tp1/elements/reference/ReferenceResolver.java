package ar.fiuba.tdd.tp1.elements.reference;

/**
 * Defines the method to be implemented by the reference class
 * Created by jonathan on 26/09/15.
 */
public interface ReferenceResolver<T> {
    T solve();
}
