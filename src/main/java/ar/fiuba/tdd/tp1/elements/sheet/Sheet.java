package ar.fiuba.tdd.tp1.elements.sheet;

import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.cell.CellIdIterator;
import ar.fiuba.tdd.tp1.elements.cell.CellRange;
import ar.fiuba.tdd.tp1.elements.reference.ColumnComparator;

import java.util.*;

/**
 * Representation of a sheet.
 * Created by matias on 9/19/15.
 */
public class Sheet {

    private static final Comparator<String> COLS_COMPARATOR = new ColumnComparator();
    private String name;
    private CellRange workingRange;

    private Map<Integer, Map<String, Cell>> cells; //cols x rows

    public Sheet() {
        this("New Sheet");
        workingRange = new CellRange("A", 1, "A", 1);
    }

    public Sheet(String name) throws IllegalArgumentException {
        checkEmptyName(name);
        this.name = name;
        this.cells = new HashMap<>();
        workingRange = new CellRange("A", 1, "A", 1);
    }

    public Cell getCell(String col, Integer row) {
        //If it doesn't exist yet, it creates the new cell.
        col = col.toUpperCase();
        if (!cellExists(col, row)) {
            createCell(col, row);
        }
        return this.cells.get(row).get(col);
    }

    public Boolean cellExists(String col, Integer row) {
        validateCellRange(row);
        col = col.toUpperCase();
        if (this.cells.containsKey(row)) {
            if (this.cells.get(row).containsKey(col)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    private Cell createCell(String col, Integer row) {
        Cell newCell = new Cell(col, row);
        if (!this.cells.containsKey(row)) {
            this.cells.put(row, new HashMap<>());
        }
        updateWorkingRange(col, row);
        this.cells.get(row).put(col, newCell);
        return newCell;
    }

    private void validateCellRange(Integer column) {
        if (column < 1) {
            throw new InvalidCellRangeException();
        }
    }


    private void checkEmptyName(String name) throws IllegalArgumentException {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Invalid Name. Should not be empty.");
        }
    }

    public String getName() {
        return this.name;
    }

    public void changeName(String newName) throws IllegalArgumentException {
        checkEmptyName(newName);
        this.name = newName;
    }

    public Iterator<Cell> visitedCellsIterator() {
        Collection<Map<String, Cell>> values = cells.values();
        List<Cell> cellResult = new ArrayList<>();
        for (Map<String, Cell> map : values) {
            Collection<Cell> values1 = map.values();
            cellResult.addAll(values1);
        }
        return cellResult.iterator();
    }

    public Iterator<Iterator<Cell>> rangeIterator(CellRange range) {
        if (range == null) {
            throw new IllegalArgumentException("Not allowed null range");
        }
        List<Iterator<Cell>> iterators = new ArrayList<>();
        for (int i = range.getStartingRow(); i <= range.getEndingRow(); i++) {
            iterators.add(new RowIterrator(i, range.getStartingCol(), range.getEndingCol()));
        }
        return iterators.iterator();
    }

    public CellRange getWorkingRange() {
        return workingRange;
    }

    public Iterator<Iterator<Cell>> workingCellRangeIterator() {
        return this.rangeIterator(getWorkingRange());
    }

    private void updateWorkingRange(String col, Integer row) {
        if (workingRange == null) {
            workingRange = new CellRange(col, row, col, row);
        } else {
            String minCol = getMinCol(col);
            String maxCol = getMaxCol(col);
            Integer minRow = getMinRow(row);
            Integer maxRow = getMaxRow(row);
            workingRange = new CellRange(minCol, minRow, maxCol, maxRow);
        }

    }

    private Integer getMaxRow(Integer row) {
        return workingRange.getEndingRow() > row ? workingRange.getEndingRow() : row;
    }

    private Integer getMinRow(Integer row) {
        return workingRange.getStartingRow() < row ? workingRange.getStartingRow() : row;
    }

    private String getMaxCol(String col) {
        return COLS_COMPARATOR.compare(col, workingRange.getEndingCol()) > 0 ? col :
                workingRange.getEndingCol();
    }

    private String getMinCol(String col) {
        return COLS_COMPARATOR.compare(col, workingRange.getStartingCol()) < 0 ? col
                : workingRange.getStartingCol();
    }

    private class RowIterrator implements Iterator<Cell> {

        private CellIdIterator idIterator;


        public RowIterrator(Integer row, String startingCol, String endingCol) {
            idIterator = new CellIdIterator(new CellRange(startingCol, row, endingCol, row));
        }

        @Override
        public boolean hasNext() {
            return idIterator.hasNext();
        }

        @Override
        public Cell next() {
            idIterator.next();
            String currentColumn = idIterator.getCurrentColumn();
            Integer currentRow = idIterator.getCurrentRow();
            if (cellExists(currentColumn, currentRow)) {
                return getCell(currentColumn, currentRow);
            }
            return new Cell(currentColumn, currentRow);
        }
    }
}
