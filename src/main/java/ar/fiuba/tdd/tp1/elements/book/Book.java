package ar.fiuba.tdd.tp1.elements.book;

import ar.fiuba.tdd.tp1.elements.sheet.Sheet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * It is the book representation..
 */
public class Book {

    private String name;
    private Sheet activeSheet;
    private HashMap<String, Sheet> sheets;

    public Book() {
        this("default");
    }

    public Book(String name) {
        this.name = name;
        this.sheets = new HashMap<>();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String newName) {
        this.name = newName;
    }


    public void createSheet(String sheetId) {
        if (this.sheets.containsKey(sheetId)) {
            throw new AlreadyExistentSheetException();
        }
        Sheet value = new Sheet(sheetId);
        this.sheets.put(sheetId, value);
        activeSheet = value;
    }

    public Sheet getSheet(String sheetId) {
        Sheet sheet = this.sheets.get(sheetId);
        if (sheet == null) {
            throw new InexistentSheetException();
        }
        return sheet;
    }

    public void deleteSheet(String sheetId) {
        if (!this.sheets.containsKey(sheetId)) {
            throw new InexistentSheetException();
        }
        this.sheets.remove(sheetId);
    }

    public List<String> getAvailableSheets() {
        List<String> keysList = new ArrayList<>();
        keysList.addAll(this.sheets.keySet());
        return keysList;
    }

    public Iterator<Sheet> iterator() {
        return sheets.values().iterator();
    }

    public Sheet getActiveSheet() {
        return activeSheet;
    }

    public void setActiveSheet(String sheet) {
        activeSheet = getSheet(sheet);
    }

}
