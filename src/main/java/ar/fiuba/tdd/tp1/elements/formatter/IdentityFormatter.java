package ar.fiuba.tdd.tp1.elements.formatter;

/**
 * Created by jonathan on 14/10/15.
 */
public class IdentityFormatter implements Formatter {

    public static final String TYPE = "IDENTITY";

    public static FormatInterpreter interpreter() {
        return (String format) ->
                new IdentityFormatter();
    }

    @Override
    public String format(String value) {
        return value;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public String getFormat() {
        return "";
    }

    @Override
    public void changeProperty(String property, String value) {
        throw new IllegalArgumentException();
    }
}
