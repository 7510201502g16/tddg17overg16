package ar.fiuba.tdd.tp1.elements.printer;

import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;

import java.util.Iterator;

/**
 * Created by jonathan on 10/10/15.
 */
public abstract class SheetPrinter implements Printer {


    private Sheet sheet;

    public SheetPrinter(Sheet sheet) {
        this.sheet = sheet;
    }

    protected abstract void printHeader(Sheet sheet);

    protected abstract void printRowEnd();

    protected abstract void printRowStart(Integer row);

    protected abstract Printer getCellPrinter(Cell cell);

    protected abstract Iterator<Iterator<Cell>> cellIterator(Sheet sheet);

    protected abstract void printCellSeparator();


    public void print() {
        printHeader(sheet);
        Iterator<Iterator<Cell>> iterator = cellIterator(sheet);
        while (iterator.hasNext()) {
            Iterator<Cell> cellIterator = iterator.next();
            boolean first = true;
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                first = startRow(first, cell);
                getCellPrinter(cell).print();
                separeteCell(cellIterator);
            }
            rowEnd(iterator);
        }
    }

    private void rowEnd(Iterator<Iterator<Cell>> iterator) {
        if (iterator.hasNext()) {
            printRowEnd();
        }
    }

    private boolean startRow(boolean first, Cell cell) {
        if (first) {
            printRowStart(cell.getRow());
            first = false;
        }
        return first;
    }

    private void separeteCell(Iterator<Cell> cellIterator) {
        if (cellIterator.hasNext()) {
            printCellSeparator();
        }
    }

    protected Sheet getSheet() {
        return sheet;
    }
}