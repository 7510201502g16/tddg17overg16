package ar.fiuba.tdd.tp1.userinterface.sheetoptions;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.elements.book.AlreadyExistentSheetException;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

/**
 * Created by jonathan on 22/10/15.
 */
public class CreateSheetOption extends MenuOption {

    private SpreadSheet spreadSheet;

    public CreateSheetOption(SpreadSheet spreadSheet) {
        super("Create sheet", "Please enter new sheet name", true);
        this.spreadSheet = spreadSheet;
    }

    @Override
    protected String executeSafeErrors(String input) {
        final Action action = ActionFactory.bookAction().createSheet(input);
        try {
            spreadSheet.execute(action);
        } catch (AlreadyExistentSheetException e) {
            return "Already exists a sheet with name\"" + getTitle() + "\"";
        }
        return "";
    }
}
