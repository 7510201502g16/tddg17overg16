package ar.fiuba.tdd.tp1.userinterface.miscoptions;

import ar.fiuba.tdd.tp1.userinterface.ContentPrinter;
import ar.fiuba.tdd.tp1.userinterface.celloptions.CellOption;

/**
 * Created by jonathan on 22/10/15.
 */
public class VisibleZoneOption extends CellOption {
    private ContentPrinter printer;

    public VisibleZoneOption(ContentPrinter printer) {
        super("Visible Zone Cell Start", "", false, null);
        this.printer = printer;
    }

    @Override
    protected String executeSafeErrors(String input) {
        printer.setCol(this.col);
        printer.setRow(this.row);
        return "";
    }
}
