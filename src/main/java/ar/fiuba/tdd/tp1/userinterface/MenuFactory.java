package ar.fiuba.tdd.tp1.userinterface;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.formatter.CurrencySymbol;
import ar.fiuba.tdd.tp1.formatter.DateFormat;
import ar.fiuba.tdd.tp1.userinterface.bookoptions.ChangeActiveBookOption;
import ar.fiuba.tdd.tp1.userinterface.bookoptions.CreateBookOption;
import ar.fiuba.tdd.tp1.userinterface.bookoptions.OpenBookOption;
import ar.fiuba.tdd.tp1.userinterface.bookoptions.SaveBookOption;
import ar.fiuba.tdd.tp1.userinterface.celloptions.ChangeValueOption;
import ar.fiuba.tdd.tp1.userinterface.celloptions.DateFormatterOption;
import ar.fiuba.tdd.tp1.userinterface.celloptions.MoneyFormatterOption;
import ar.fiuba.tdd.tp1.userinterface.celloptions.NumberFormatterOption;
import ar.fiuba.tdd.tp1.userinterface.miscoptions.RedoOption;
import ar.fiuba.tdd.tp1.userinterface.miscoptions.UndoOption;
import ar.fiuba.tdd.tp1.userinterface.miscoptions.VisibleZoneOption;
import ar.fiuba.tdd.tp1.userinterface.sheetoptions.CreateSheetOption;
import ar.fiuba.tdd.tp1.userinterface.sheetoptions.ExportSheetOption;
import ar.fiuba.tdd.tp1.userinterface.sheetoptions.ImportSheetOption;
import ar.fiuba.tdd.tp1.userinterface.sheetoptions.SelectListSheetOption;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jonathan on 22/10/15.
 */
public class MenuFactory {

    public static MenuOption createRootMenu(ContentPrinter printer, SpreadSheet spreadSheet) {
        final CompositeMenuOption compositeMenuOption = new CompositeMenuOption("Menu Options",
                Arrays.asList(createBookMenu(spreadSheet), createCellMenu(spreadSheet), createSheetMenu(spreadSheet),
                        new VisibleZoneOption(printer), new UndoOption(spreadSheet), new RedoOption(spreadSheet)));
        return compositeMenuOption;
    }

    private static MenuOption createBookMenu(SpreadSheet spreadSheet) {
        List<MenuOption> options = new ArrayList<>();
        options.add(new CreateBookOption(spreadSheet));
        options.add(new ChangeActiveBookOption(spreadSheet));
        options.add(new SaveBookOption(spreadSheet));
        options.add(new OpenBookOption(spreadSheet));
        final CompositeMenuOption compositeMenuOption = new CompositeMenuOption("Book Menu", options);
        return compositeMenuOption;
    }

    private static MenuOption createCellMenu(SpreadSheet spreadSheet) {
        MenuOption changeValue = new ChangeValueOption(spreadSheet);
        MenuOption changeformater = new CompositeMenuOption("Cell format", createFormatterOptions(spreadSheet));
        return new CompositeMenuOption("Cell Menu", Arrays.asList(changeValue, changeformater));
    }

    private static List<MenuOption> createFormatterOptions(SpreadSheet spreadSheet) {
        List<MenuOption> options = new ArrayList<>();
        options.addAll(createMoneyFormatters(spreadSheet));
        options.addAll(createDateFormatters(spreadSheet));
        options.add(new NumberFormatterOption(spreadSheet));
        return options;

    }

    private static List<MenuOption> createMoneyFormatters(SpreadSheet spreadSheet) {
        List<MenuOption> options = new ArrayList<>();
        final CurrencySymbol[] values = CurrencySymbol.values();
        for (int i = 0; i < values.length; i++) {
            CurrencySymbol value = values[i];
            options.add(new MoneyFormatterOption(value, spreadSheet));
        }
        return options;
    }

    private static List<MenuOption> createDateFormatters(SpreadSheet spreadSheet) {
        final DateFormat[] formats = DateFormat.values();
        List<MenuOption> options = new ArrayList<>();
        for (int i = 0; i < formats.length; i++) {
            DateFormat format = formats[i];
            options.add(new DateFormatterOption(format, spreadSheet));
        }
        return options;
    }

    private static MenuOption createSheetMenu(SpreadSheet spreadSheet) {
        final CreateSheetOption createSheetOption = new CreateSheetOption(spreadSheet);
        final SelectListSheetOption sheetOption = new SelectListSheetOption(spreadSheet);
        final ExportSheetOption export = new ExportSheetOption(spreadSheet);
        final ImportSheetOption importSheet = new ImportSheetOption(spreadSheet);
        return new CompositeMenuOption("Sheet Menu", Arrays.asList(createSheetOption, sheetOption, export, importSheet));

    }
}
