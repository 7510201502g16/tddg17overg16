package ar.fiuba.tdd.tp1.userinterface.bookoptions;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.elements.printer.PrintException;
import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.queries.Query;
import ar.fiuba.tdd.tp1.queries.factory.QueryFactory;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

/**
 * Created by jonathan on 22/10/15.
 */
public class SaveBookOption extends MenuOption {

    private SpreadSheet spreadSheet;

    @Override
    protected String executeSafeErrors(String input) {
        final Query<Printer> query = QueryFactory.printQuery().bookToFile(input);
        String error = getException(query);
        if (error != null) {
            return error;
        }
        return "";
    }

    private String getException(Query<Printer> query) {
        try {
            spreadSheet.execute(query).print();

        } catch (PrintException e) {
            return e.getMessage();
        }
        return null;
    }

    public SaveBookOption(SpreadSheet spreadSheet) {
        super("Save active Book", "Please enter filename", true);
        this.spreadSheet = spreadSheet;
    }
}
