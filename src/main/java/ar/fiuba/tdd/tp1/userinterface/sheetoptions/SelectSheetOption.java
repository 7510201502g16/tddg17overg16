package ar.fiuba.tdd.tp1.userinterface.sheetoptions;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

/**
 * Created by jonathan on 22/10/15.
 */
public class SelectSheetOption extends MenuOption {

    private SpreadSheet spreadSheet;

    public SelectSheetOption(String sheetId, SpreadSheet spreadSheet) {
        super(sheetId, "", false);
        this.spreadSheet = spreadSheet;
    }

    @Override
    protected String executeSafeErrors(String input) {
        spreadSheet.setActiveSheetId(getTitle());
        return "";
    }
}
