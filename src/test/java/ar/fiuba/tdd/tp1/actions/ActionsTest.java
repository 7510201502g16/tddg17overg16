package ar.fiuba.tdd.tp1.actions;

import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.api.IllegalActionException;
import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.book.InexistentSheetException;
import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.formatter.Formatter;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.formatter.CurrencySymbol;
import ar.fiuba.tdd.tp1.formatter.MoneyFormatter;
import ar.fiuba.tdd.tp1.formulas.formulacreator.FormulaCreator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by maxi on 01/10/15.
 */

public class ActionsTest {

    private Action genericAction;
    private Book book;

    @Before
    public void before() {
        genericAction = new Action() {
        };
        book = new Book();
        genericAction.setContext(book);
    }

    @Test(expected = InexistentSheetException.class)
    public void createNewSheetAction() {
        Action createSheetAction = ActionFactory.bookAction().createSheet("New sheet");
        createSheetAction.setContext(book);
        createSheetAction.execute();
        assertEquals("New sheet", book.getSheet("New sheet").getName());

        createSheetAction.undo();
        book.getSheet("New sheet");
    }


    @Test
    public void changeCellFormulaAction() {
        book.createSheet("New sheet");
        Sheet sheet = book.getSheet("New sheet");
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));

        Action changeCellFormulaAction = ActionFactory.cellAction("A", 1, "New sheet").changeFormula("2");
        changeCellFormulaAction.setContext(book);
        changeCellFormulaAction.execute();
        assertEquals("2.0", book.getSheet("New sheet").getCell("A", 1).getValue());
        changeCellFormulaAction.undo();
        assertEquals("1.0", book.getSheet("New sheet").getCell("A", 1).getValue());
    }


    @Test
    public void changeCellFormatterAction() {
        book.createSheet("New sheet");
        MoneyFormatter formatter = MoneyFormatter.noDecimalsFormatter(CurrencySymbol.YEN);
        Action action = ActionFactory.cellAction("A", 1, "New sheet").changeFormatter(formatter);
        Cell cell = book.getSheet("New sheet").getCell("A", 1);

        final Formatter prevFm = cell.getFormatter();
        action.setContext(book);
        action.execute();
        assertEquals(formatter, cell.getFormatter());
        action.undo();
        assertEquals(prevFm, cell.getFormatter());
    }


    @Test(expected = IllegalActionException.class)
    public void executeActionWithoutContext() {
        Action action = new Action() {
        };
        action.execute();
    }

    @Test(expected = IllegalActionException.class)
    public void executeTwoTimesAction() {
        genericAction.execute();
        genericAction.execute();
    }


    @Test
    public void executeUndoAndRedo() {
        genericAction.execute();
        genericAction.undo();
        genericAction.execute();
    }


    @Test(expected = IllegalActionException.class)
    public void undoNotExecuted() {
        genericAction.undo();
    }


}
