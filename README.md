#Enunciado
Se deberá agregar las siguientes funcionalidades al TP recibido para la tercer iteración. La url del repositorio será enviado al channel de cada grupo de slack.
Se deberá hacer un análisis del trabajo práctico recibido sobre el cual se desarrollarán las nuevas funcionalidades pedidas.
#Funcionalidad a Implementar
1. Operador ^ Devuelve su primer argumento elevado a su segundo argumento. Ej: =3^2 o =A1^A2
2. =TODAY() Asigna la fecha actual
3. =LEFT(A1, 3) Ej: A1=”Hola” LEFT(A1,3) = LEFT(“Hola”,3) = “Hol”
4. =RIGHT(A1,3) Idem LEFT
5. =PRINTF(“Patron: $0”, A1) Completa un patrón insertando valores dados en argumentos sucesivos. Ej: A1=Juan A2=Perez A3=PRINTF(“Hola $0 $1”, A1, A2) A3 = "Hola Juan Perez"
6. Poder definir Named Ranges, por ejemplo poder definir RangeA = A1:A10 y luego usar RangeA como rango en las funciones que soportan rangos. Ej: MAX(RangoA) seria equivalente a MAX(A1:A10)
7. Soportar todas estas operaciones desde el REPL, y contar con test unitarios para las mismas.

#Objetivos
* Implementar a la herramienta las nuevas funcionalidades requeridas.
* Aplicar las técnicas vistas en la teoría y en la práctica.
* **IMPORTANTE: Se deberá realizar un análisis del diseño y código recibido. Dicho análisis deberá ser informado en un reporte y deberá contemplar la influencia del diseño y código sobre la implementación de la funcionalidad pedida. Ejemplos: si el código recibido violaba algún principio SOLID, presentaba code smells, malas prácticas, patrones mal implementados, documentación insuficiente o incorrecta, tests o builds rotos, etc.**